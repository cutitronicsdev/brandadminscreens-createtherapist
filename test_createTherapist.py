import pymysql
import logging
import json
import os
import jsonschema
import pytest
import unittest
import botocore
from createTherapist import lambda_handler
from createTherapist import fn_checkBrandExist, fn_CheckTherapistExists, fn_createTherapist, fn_createCognitoUser

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection

    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'],
                                     os.environ['v_password'], os.environ['v_database'], connect_timeout=5)
    except Exception as e:
        logger.error(e)
        logger.error(
            "ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def createTherapist(payload):
    class context:
        def __init__(self):
            self.function_name = "createTherapist"

    context = context()
    event = payload
    return(lambda_handler(event, context))


def test_lambda_handler_ValidcheckBrandExist():

    logger.info(" ")
    logger.info("Testing fn_checkBrandExist")
    logger.info(" ")

    logger.info(" ")
    logger.info("Valid Brand ID")
    logger.info(" ")

    event = {
        'CutitronicsBrandID': "000PPOJ2",
        "CutitronicsTherapistID": "ishga_Therapist12",
        "TherapistName": "Stan Kostadinov",
        "TherapistEmail": "sbkostadinov@gmail.com"
    }

    lv_result = createTherapist(event)
    lv_result = json.loads(lv_result.get('body'))

    lv_result = (fn_checkBrandExist("0002"))

    assert lv_result.get('BrandName') == 'ishga'


def test_lambda_handler_InvalidcheckBrandExist():

    logger.info(" ")
    logger.info("Testing fn_checkBrandExist")
    logger.info(" ")

    logger.info(" ")
    logger.info("Valid Brand ID")
    logger.info(" ")

    event = {
        'CutitronicsBrandID': "+000(*hJ",
        "CutitronicsTherapistID": "ishga_Therapist12",
        "TherapistName": "Stan Kostadinov",
        "TherapistEmail": "sbkostadinov@gmail.com"
    }

    lv_result = createTherapist(event)
    lv_result = json.loads(lv_result.get('body'))

    lv_result = (fn_checkBrandExist("+000(*hJ"))
    assert lv_result == 500


def test_lambda_handler_ValidcheckTherapistExists():

    logger.info(" ")
    logger.info("Valid CutitronicsTherapistID")
    logger.info(" ")

    event = {
        'CutitronicsBrandID': "+000(*hJ",
        "CutitronicsTherapistID": "ishga_Therapist9",
        "TherapistName": "Stan Kostadinov",
        "TherapistEmail": "sbkostadinov@gmail.com"
    }

    lv_result = createTherapist(event)
    lv_result = json.loads(lv_result.get('body'))

    lv_result = (fn_CheckTherapistExists("ishga_Therapist9", "0002"))
    assert lv_result.get('TherapistName') == "ishga_Therapist9"


def test_lambda_handler_InValidcheckTherapistExists():

    logger.info(" ")
    logger.info("Invalid CutitronicsTherapistID")
    logger.info(" ")

    event = {
        'CutitronicsBrandID': "+000(*hJ",
        "CutitronicsTherapistID": "ishga_Therapist11",
        "TherapistName": "Stan Kostadinov",
        "TherapistEmail": "sbkostadinov@gmail.com"
    }

    lv_result = createTherapist(event)
    lv_result = json.loads(lv_result.get('body'))

    lv_result = (fn_CheckTherapistExists("ishga_Therapist11", "0002"))
    assert lv_result == 300


def test_lambda_handler_createTherapist():

    logger.info(" ")
    logger.info("Create CutitronicsTherapist")
    logger.info(" ")

    openConnection()

    lv_statement = "DELETE FROM TherapistDetails WHERE CutitronicsTherapistID = 'ishga_Therapist12';"

    with Connection.cursor() as cursor:
        cursor.execute(lv_statement)
        Connection.commit()
        Connection.close()

    event = {
        'CutitronicsBrandID': "+000KILO^&",
        "CutitronicsTherapistID": "ishga_Therapist12",
        "TherapistName": "Stan",
        "TherapistEmail": "sbkostadinov@gmail.com"
    }

    lv_result = createTherapist(event)
    lv_result = json.loads(lv_result.get('body'))

    # Create ishga_Therapist12

    fn_createTherapist("0002", "ishga_Therapist12", "Stan")

    lv_result = (fn_CheckTherapistExists("ishga_Therapist12", "0002"))
    assert lv_result.get("TherapistName") == "Stan"


def test_lambda_handler_InvalidcreateCognitoUser():

    logger.info(" ")
    logger.info("testing fn_createCognitoUser")
    logger.info(" ")

    event = {
        'CutitronicsBrandID': "+000KILO^&",
        "CutitronicsTherapistID": "ishga_Therapist12",
        "TherapistName": "Joshn",
        "TherapistEmail": "sbkostadinov@gmail.com"
    }

    lv_result = createTherapist(event)
    lv_result = json.loads(lv_result.get('body'))

    # Create CognitoUser sbkostadinov@gmail.com

    lv_result = (fn_createCognitoUser("sbkostadinov@gmail.com"))
    assert lv_result == 500


def test_lambda_handler_validcreateCognitoUser():

    logger.info(" ")
    logger.info("testing fn_createCognitoUser")
    logger.info(" ")

    event = {
        'CutitronicsBrandID': "+000KILO^&",
        "CutitronicsTherapistID": "ishga_Therapist12",
        "TherapistName": "Joshn",
        "TherapistEmail": "sbkostadinov@gmail.com"
    }

    lv_result = createTherapist(event)
    lv_result = json.loads(lv_result.get('body'))

    # Create CognitoUser sbkostadinov@gmail.com

    lv_result = (fn_createCognitoUser("sbksuccess87654@gmail.com"))
    assert lv_result.get("User")['Username'] == "sbksuccess87654@gmail.com"


def test_lambda_handler():

    logger.info(" ")
    logger.info("Test payload")
    logger.info(" ")

    event = {
        'CutitronicsBrandID': "0002",
        "CutitronicsTherapistID": "ishga_Therapist423x",
        "TherapistName": "Stan",
        "TherapistEmail": "sbkostadinov13@gmail.com"
    }

    lv_result = createTherapist(event)
    lv_result = json.loads(lv_result.get('body'))

    assert lv_result.get("Status") == "Success"


def test_schema_200():

    event = {

        "CutitronicsBrandID": "0002",
        "CutitronicsTherapistID": "ishga_Therapist140",
        "TherapistName": "Stan Kostadinov",
        "TherapistEmail": "sbkostadinov@gmail.com"

    }

    lv_result = createTherapist(event)
    lv_result = json.loads(lv_result.get('body'))
    lv_result = json.dumps(lv_result)

    lv_schema = open(os.path.join(os.path.dirname(__file__),
                                  'createTherapist_200_response.json'), 'r').read()
    lv_schema = json.loads(lv_schema)
    try:
        jsonschema.validate(instance=lv_result, schema=lv_schema,
                            format_checker=jsonschema.FormatChecker())
    except jsonschema.exceptions.ValidationError as e:
        print("well-formed but invalid JSON: ", e)
    except json.decoder.JSONDecodeError as e:
        print("poorly-formed text, not JSON: ", e)


def test_schema_400():

    event = {
        'CutitronicsBrandID': "0002",
        "CutitronicsTherapistID": "ishga_Therapist14",
        "TherapistName": "Stan Kostadinov",
        "TherapistEmail": "sbkostadinov@gmail.com"
    }

    lv_result = createTherapist(event)
    lv_result = json.loads(lv_result.get('body'))
    lv_result = json.dumps(lv_result)

    lv_schema = open(os.path.join(os.path.dirname(__file__),
                                  'createTherapist_400_response.json'), 'r').read()
    lv_schema = json.loads(lv_schema)
    try:
        jsonschema.validate(instance=lv_result, schema=lv_schema,
                            format_checker=jsonschema.FormatChecker())
    except jsonschema.exceptions.ValidationError as e:
        print("well-formed but invalid JSON: ", e)
    except json.decoder.JSONDecodeError as e:
        print("poorly-formed text, not JSON: ", e)
