'''
 file Name - test_0001_InvalidSession
 Date - 06/10/2021
 Test Case - Tests if Session exists
 Jira ticket - N/A
'''

import logging
import json
import jsonschema
import os
from api import createAdmin
import pytest

logger = logging.getLogger()
logger.setLevel(logging.INFO)


@pytest.fixture()
def lambda_event():
    return {

        "TherapistEmail": "sbkostadinov@gmail.com"
    }


@pytest.fixture()
def lambda_context():
    return MockContext()


class MockContext:
    def __init__(self):
        self.function_name = "createAdmin"
        self.log_group_name = '/aws/lambda/createAdmin'
        self.aws_request_id = '1'
        self.memory_limit_in_mb = 128


def test_handler(lambda_event, lambda_context):

    logger.info(" ")
    logger.info("Valid Payload")
    logger.info(" ")

    lv_result = createAdmin.lambda_handler(lambda_event, lambda_context)
    lv_result = json.loads(lv_result.get('body'))

    lv_result = json.dumps(lv_result)

    lv_schema = open(os.path.join(os.path.dirname(__file__), 'createAdmin_200_response.json'), 'r').read()
    lv_schema = json.loads(lv_schema)
    try:
        jsonschema.validate(instance=lv_result, schema=lv_schema, format_checker=jsonschema.FormatChecker())
    except jsonschema.exceptions.ValidationError as e:
        print("well-formed but invalid JSON: ", e)
    except json.decoder.JSONDecodeError as e:
        print("poorly-formed text, not JSON: ", e)
