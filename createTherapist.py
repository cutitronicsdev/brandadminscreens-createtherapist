import logging
import traceback
import datetime
import json
from botocore import errorfactory
import pymysql
import os
import sys
import boto3

from time import sleep

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection

    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'],
                                     os.environ['v_password'], os.environ['v_database'], connect_timeout=5)
    except Exception as e:
        logger.error(e)
        logger.error(
            "ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_checkBrandExist(lv_CutitronicsBrandID):

    lv_list = ""
    lv_statement = "SELECT CutitronicsBrandID, BrandName, ContactName, ContactTitle, ContactEmail FROM BrandDetails WHERE CutitronicsBrandID = %(CutitronicsBrandID)s"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(
                lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if len(lv_list) == 0:
                return(500)
            else:

                return lv_list

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 500


def fn_createCognitoUser(lv_email, lv_poolID):
    # Script to create dummy user accounts to aid with testing export :)
    # https://docs.aws.amazon.com/cli/latest/reference/cognito-idp/admin-create-user.html

    try:

        client = boto3.client('cognito-idp')

        lv_numberOfAcounts = 1  # Number of Accounts
        lv_start = 0

        while lv_start < lv_numberOfAcounts:
            # register a therapist/user in CognitoUserPools with provided email
            lv_userName = lv_email
            # value of response could be the lv_email by itself if used as an attribute
            response = client.admin_create_user(UserPoolId=lv_poolID, Username=lv_userName, UserAttributes=[
                                                {"Name": "email", "Value": lv_userName}, {"Name": "email_verified", "Value": "true"}])
            sleep(0.5)  # Sleep per account
            lv_start += 1  # Increase count
            print(response)
        return response

    except Exception as e:
        logging.info(e)
        return 500


def fn_CheckTherapistExists(lv_CutitronicsTherapistID, lv_CutitronicsBrandID):

    lv_list = ""
    lv_statement = "SELECT CutitronicsTherapistID, CutitronicsBrandID, CutitronicsLocationID, TherapistName FROM TherapistDetails WHERE CutitronicsTherapistID = %(CutitronicsTherapistID)s AND CutitronicsBrandID = %(CutitronicsBrandID)s"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {
                           'CutitronicsTherapistID': lv_CutitronicsTherapistID, 'CutitronicsBrandID': lv_CutitronicsBrandID})
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = (dict(zip(field_names, row)))

            if len(lv_list) == 0:
                return(300)
            else:
                return lv_list

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 300


def fn_createTherapist(lv_CutitronicsBrandID, lv_CutitronicsTherapistID, lv_CutitronicsTherapistName):

    lv_statement = "INSERT INTO `TherapistDetails` (`CutitronicsTherapistID`, `CutitronicsBrandID`, `CutitronicsLocationID`, `TherapistName`, `CreationDate`, `CreationUser`, `LastUpdateDate`, `LastUpdateUser`) VALUES (%(CutitronicsTherapistID)s, %(CutitronicsBrandID)s, NULL, %(TherapistName)s, SYSDATE(), 'root', NULL, NULL); "

    with Connection.cursor() as cursor:
        cursor.execute(lv_statement, {"CutitronicsTherapistID": lv_CutitronicsTherapistID,
                                      "CutitronicsBrandID": lv_CutitronicsBrandID, "TherapistName": lv_CutitronicsTherapistName})

        if cursor.rowcount == 1:
            return 100

        else:
            logger.warning("Could not INSERT into DataBase")
            Connection.rollback()
            return 400


def lambda_handler(event, context):

    openConnection()

    logger.info(" ")
    logger.info("Logging for function - %s - %s",
                context.function_name, datetime.date.today())
    logger.info(" ")
    logger.info("Event - %s", event)
    logger.info(" ")

    # context Dump

    logger.info(" ")
    logger.info("Context.function_name - %s", context.function_name)
    logger.info(" ")

    # Variables
    lv_CutitronicsBrandID = None
    lv_CutitronicsTherapistID = None
    lv_CutitronicsTherapistName = None
    lv_CutitronicsTherapistEmail = None
    lv_userPool = os.environ['v_userPool']

    # Return block
    createTherapist_out = {"Service": context.function_name, "Status": "Success",
                           "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {}}

    try:

        try:
            body = json.loads(event['body'])
        except KeyError as e:
            try:
                logger.info("Body type exception: %s", e.args)
                body = event
            except TypeError:
                app_json = json.dumps(event)
                event = {"body": app_json}
                body = json.loads(event['body'])

        logger.info(" ")
        logger.info("Payload body - '{body}'".format(body=body))
        logger.info(" ")

        lv_CutitronicsBrandID = body.get('CutitronicsBrandID')
        lv_CutitronicsTherapistID = body.get('CutitronicsTherapistID')
        lv_CutitronicsTherapistName = body.get('TherapistName')
        lv_CutitronicsTherapistEmail = body.get('TherapistEmail')

        '''
        1. Does brand exist ?
        '''

        logger.info(" ")
        logger.info("Calling fn_checkBrandExist with the following input {CutitronicsBrandID}".format(
            CutitronicsBrandID=lv_CutitronicsBrandID))
        logger.info(" ")

        lv_BrandType = fn_checkBrandExist(lv_CutitronicsBrandID)

        logger.info(" ")
        logger.info(
            "fn_checkBrandExist return - {lv_BrandType}".format(lv_BrandType=lv_BrandType))
        logger.info(" ")

        if lv_BrandType == 500:  # Failure

            logger.error("Brand '{lv_CutitronicsBrandID}' has not been found in the back office systems".format(
                lv_CutitronicsBrandID=lv_CutitronicsBrandID))

            createTherapist_out['Status'] = "Error"
            # getCartridgeID_001
            createTherapist_out['ErrorCode'] = context.function_name + "_001"
            createTherapist_out['ErrorDesc'] = "Supplied brandID does not exist in the BO Database"
            createTherapist_out["ServiceOutput"]["CutitronicsBrandID"] = lv_CutitronicsBrandID
            createTherapist_out["ServiceOutput"]["CutitronicsTherapistID"] = lv_CutitronicsTherapistID
            createTherapist_out["ServiceOutput"]["TherapistName"] = lv_CutitronicsTherapistName

            # Lambda response
            logger.error("Unable to complete execution, Brand doesn't exist. Return: '{lv_getCartIDOut}'".format(
                lv_getCartIDOut=json.dumps(createTherapist_out)))

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(createTherapist_out)
            }

        # """
        # 2. If CutitronicsTherapistID does not already exist in DB or CognitoUserPool create an entry for him/her
        # """
        lv_TherapistType = fn_CheckTherapistExists(
            lv_CutitronicsTherapistID, lv_CutitronicsBrandID)

        # Check if user email has been registered in CognitoUserPools
        client = boto3.client('cognito-idp')

        lv_filter = 'email="{lv_email}"'.format(lv_email=lv_CutitronicsTherapistEmail)
        response = client.list_users(UserPoolId=lv_userPool, AttributesToGet=['email'], Filter=lv_filter)

        if lv_TherapistType == 300 and (len(response.get('Users')) == 0):

            # Lambda response
            logger.info(" '{lv_TherapistID}' has not been found in BackOffice".format(
                lv_TherapistID=lv_CutitronicsTherapistID))

            fn_createTherapist(lv_CutitronicsBrandID, lv_CutitronicsTherapistID, lv_CutitronicsTherapistName)

            fn_createCognitoUser(lv_CutitronicsTherapistEmail, lv_userPool)
            createTherapist_out['Status'] = "Success"
            createTherapist_out['ErrorCode'] = ""
            createTherapist_out['ErrorDesc'] = ""
            createTherapist_out["ServiceOutput"]["CutitronicsBrandID"] = lv_CutitronicsBrandID
            createTherapist_out["ServiceOutput"]["CutitronicsTherapistID"] = lv_CutitronicsTherapistID
            createTherapist_out["ServiceOutput"]["TherapistName"] = lv_CutitronicsTherapistName

            # Lambda has successfully executed
            Connection.commit()
            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 200,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(createTherapist_out)
            }

        else:

            # Lambda response
            logger.error(" '{lv_TherapistID}' already exists in the BackOffice".format(
                lv_TherapistID=lv_CutitronicsTherapistID))

            logger.error(" '{lv_TherapistEmail}' already exists in CognitoUserPool".format(
                lv_TherapistEmail=lv_CutitronicsTherapistEmail))

            createTherapist_out['Status'] = "Error"
            # getCartridgeID_001
            createTherapist_out['ErrorCode'] = context.function_name + "_002"
            createTherapist_out['ErrorDesc'] = "Therapist already exists in Database and CognitoUserPool"
            createTherapist_out["ServiceOutput"]["CutitronicsBrandID"] = lv_CutitronicsBrandID
            createTherapist_out["ServiceOutput"]["CutitronicsTherapistID"] = lv_CutitronicsTherapistID
            createTherapist_out["ServiceOutput"]["TherapistName"] = lv_CutitronicsTherapistName

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(createTherapist_out)
            }

    except Exception as e:
        logger.error(
            "CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(
            exception_type, exception_value, exception_traceback)
        err_msg = json.dumps({
            "errorType": exception_type.__name__,
            "errorMessage": str(exception_value),
            "stackTrace": traceback_string
        })
        logger.error(err_msg)
        # Populate Cutitronics reply block

        # Lambda execution was not successful
        Connection.rollback()

        createTherapist_out['Status'] = "Error"
        # regUser_000
        createTherapist_out['ErrorCode'] = context.function_name + "_000"
        createTherapist_out['ErrorDesc'] = "CatchALL"
        createTherapist_out["ServiceOutput"]["CutitronicsBrandID"] = lv_CutitronicsBrandID
        createTherapist_out["ServiceOutput"]["CutitronicsTherapistID"] = lv_CutitronicsTherapistID
        createTherapist_out["ServiceOutput"]["TherapistName"] = lv_CutitronicsTherapistName

        # Lambda response

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(createTherapist_out)
        }
