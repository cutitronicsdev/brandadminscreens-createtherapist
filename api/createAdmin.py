import logging
import traceback
import datetime
import json
from botocore import errorfactory
import pymysql
import os
import sys
import boto3
import pprint

from time import sleep

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection
    try:

        Connection = pymysql.connect(
            os.environ["v_db_host"],
            os.environ["v_username"],
            os.environ["v_password"],
            os.environ["v_database"],
            connect_timeout=5
        )

    except Exception as e:
        logger.error(e)
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_createCognitoUser(lv_email, lv_poolID):
    # Script to create dummy user accounts to aid with testing export :)
    # https://docs.aws.amazon.com/cli/latest/reference/cognito-idp/admin-create-user.html

    try:

        client = boto3.client('cognito-idp')

        lv_numberOfAcounts = 1  # Number of Accounts
        lv_start = 0

        while lv_start < lv_numberOfAcounts:
            # register a therapist/user in CognitoUserPools with provided email
            lv_userName = lv_email
            # value of response could be the lv_email by itself if used as an attribute
            response = client.admin_create_user(UserPoolId=lv_poolID, Username=lv_userName, UserAttributes=[{"Name": "email", "Value": lv_userName}, {"Name": "email_verified", "Value": "true"}])
            sleep(0.5)  # Sleep per account
            lv_start += 1  # Increase count
            print(response)
        return response

    except Exception as e:
        logging.info(e)
        return 500


def lambda_handler(event, context):

    openConnection()

    logger.info(" ")
    logger.info("Logging for function - %s - %s",
                context.function_name, datetime.date.today())
    logger.info(" ")
    logger.info("Event - %s", event)
    logger.info(" ")

    # context Dump

    logger.info(" ")
    logger.info("Context.function_name - %s", context.function_name)
    logger.info(" ")

    # Variables
    lv_CutitronicsTherapistEmail = None
    lv_userPool = "eu-west-2_rHRgC8CSV"  # os.environ['v_userPool'] provide value for Cognito pool when testing

    # Return block
    createTherapist_out = {"Service": context.function_name, "Status": "Success",
                           "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {}}

    try:

        try:
            body = json.loads(event['body'])
        except KeyError as e:
            try:
                logger.info("Body type exception: %s", e.args)
                body = event
            except TypeError:
                app_json = json.dumps(event)
                event = {"body": app_json}
                body = json.loads(event['body'])

        logger.info(" ")
        logger.info("Payload body - '{body}'".format(body=body))
        logger.info(" ")

        lv_CutitronicsTherapistEmail = body.get('TherapistEmail')

        # """
        # 2. If CutitronicsTherapistID does not already exist in DB or CognitoUserPool create an entry for him/her
        # """

        # Check if user email has been registered in CognitoUserPools
        client = boto3.client('cognito-idp')

        lv_filter = 'email="{lv_email}"'.format(lv_email=lv_CutitronicsTherapistEmail)
        response = client.list_users(UserPoolId=lv_userPool, AttributesToGet=['email'], Filter=lv_filter)

        if (len(response.get('Users')) == 0):

            # Lambda response
            fn_createCognitoUser(lv_CutitronicsTherapistEmail, lv_userPool)
            createTherapist_out['Status'] = "Success"
            createTherapist_out['ErrorCode'] = ""
            createTherapist_out['ErrorDesc'] = ""
            createTherapist_out["ServiceOutput"]["TherapistEmail"] = lv_CutitronicsTherapistEmail

            # Lambda has successfully executed
            Connection.commit()
            Connection.close()

            return {
                "isBase64Encoded": "false",
                "statusCode": 200,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(createTherapist_out)
            }

        else:

            # Lambda response
            logger.error(" '{lv_TherapistEmail}' already exists in CognitoUserPool".format(
                lv_TherapistEmail=lv_CutitronicsTherapistEmail))

            createTherapist_out['Status'] = "Error"
            # getCartridgeID_001
            createTherapist_out['ErrorCode'] = context.function_name + "_002"
            createTherapist_out['ErrorDesc'] = "Therapist already exists in CognitoUserPool"
            createTherapist_out["ServiceOutput"]["TherapistEmail"] = lv_CutitronicsTherapistEmail

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(createTherapist_out)
            }

    except Exception as e:
        logger.error(
            "CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(
            exception_type, exception_value, exception_traceback)
        err_msg = json.dumps({
            "errorType": exception_type.__name__,
            "errorMessage": str(exception_value),
            "stackTrace": traceback_string
        })
        logger.error(err_msg)
        # Populate Cutitronics reply block

        # Lambda execution was not successful
        Connection.rollback()

        createTherapist_out['Status'] = "Error"
        # regUser_000
        createTherapist_out['ErrorCode'] = context.function_name + "_000"
        createTherapist_out['ErrorDesc'] = "CatchALL"
        createTherapist_out["ServiceOutput"]["TherapistEmail"] = lv_CutitronicsTherapistEmail

        # Lambda response

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(createTherapist_out)
        }
